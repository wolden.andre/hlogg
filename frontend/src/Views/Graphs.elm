module Views.Graphs exposing (viewGraphs)

import Html exposing (Html, div, text)
import Messages exposing (Msg(..))
import Models.Model exposing (Model)


viewGraphs : Model -> Html Msg
viewGraphs model =
    div []
        [ text "Graphs"
        , viewPieChart model
        ]


viewPieChart : Model -> Html Msg
viewPieChart model =
    div [] [ text "Pie Chart" ]
