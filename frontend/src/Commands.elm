module Commands exposing (baseUrl, decodeActivities, decodeActivity, decodeRecord, decodeRecords, getActivities, getDates, getNow, getRecords, postNewRecord, postRecordBody, saveNewRecordOnDate)

import Http exposing (..)
import Json.Decode as Decode
import Json.Encode as Encode
import Messages exposing (..)
import Models.Types exposing (..)


baseUrl : String
baseUrl =
    "https://hlogg-shiny.herokuapp.com/api/hobby"
    --"https://hlogg-gcp.appspot.com/api/hobby"
    --  "http://localhost:8080/api/hobby"



-- Record On Date Commands


saveNewRecordOnDate : Int -> String -> Cmd Msg
saveNewRecordOnDate activity_id date =
      Http.post
        { url = baseUrl ++ "/records/new"
        , body = postNewRecordOnDateBody activity_id date
        , expect = Http.expectJson RecordMaybeAdded (decodeOption decodeRecord)
        }
    --Http.post {url: baseUrl ++ "/records/new_with_date", } RecordAdded (postNewRecordOnDateRequest activity_id date)

postNewRecordOnDateBody : Int -> String -> Http.Body
postNewRecordOnDateBody activity_id date =
    Encode.object
        [ ( "activity_id", Encode.int activity_id )
        , ( "date", Encode.string date )
        ]
        |> Http.jsonBody



-- Record Commands

decodeRecords : Decode.Decoder Records
decodeRecords =
    Decode.list decodeRecord


decodeOption : Decode.Decoder a -> Decode.Decoder (Option a)
decodeOption decoder =
        Decode.map3 Option
            (Decode.field "t" decoder)
            (Decode.field "empty" Decode.bool)
            (Decode.field "defined" Decode.bool)

decodeRecord : Decode.Decoder Record
decodeRecord =
    Decode.map4 Record
        (Decode.field "id" Decode.int)
        (Decode.field "date" Decode.string)
        (Decode.field "week" Decode.int)
        (Decode.field "activity"
            (Decode.map2 Activity
                (Decode.field "activity_id" Decode.int)
                (Decode.field "activity_name" Decode.string)
            )
        )


getRecords : Cmd Msg
getRecords =
          Http.get
            { url = baseUrl ++ "/records"
            , expect = Http.expectJson LoadRecords decodeRecords
            }
    --Http.send LoadRecords getRecordsRequest


postRecordBody : Int -> Http.Body
postRecordBody activity_id =
    Encode.object [ ( "activity_id", Encode.int activity_id ) ]
        |> Http.jsonBody


--postRecordRequest : Int -> Http.Request Record
--postRecordRequest activity_id =
--    Http.post (baseUrl ++ "/records/new") (postRecordBody activity_id) decodeRecord


postNewRecord : Int -> Cmd Msg
postNewRecord activity_id =
    Http.post { url = baseUrl ++ "/records/new"
                  , body = postRecordBody activity_id
                  , expect = Http.expectJson RecordAdded decodeRecord
                  }

    --Http.send RecordAdded (postRecordRequest activity_id)


--deleteRecordRequest : Int -> Http.Request Bool
--deleteRecordRequest record_id =
--    Http.request
--        { method = "DELETE"
--        , headers = []
--        , url = baseUrl ++ "/records/delete/" ++ String.fromInt record_id
--        , body = Http.emptyBody
--        , expect = Http.expectWhatever Decode.bool
--        , timeout = Nothing
--        , tracker = Nothing
--        , withCredentials = False
--        }
--
--
--deleteRecord : Int -> Cmd Msg
--deleteRecord record_id =
--    Http.post RecordDeleted (deleteRecordRequest record_id)


--postNewRecordOnDate : Int -> String -> Cmd Msg
--postNewRecordOnDate activity_id date =
--    Http.send RecordAdded (postRecordOnDateRequest activity_id date)


--postRecordOnDateRequest : Int -> String -> Http.Request Record
--postRecordOnDateRequest activity_id date =
--    Http.post (baseUrl ++ "/records/new_with_date") (postRecordBodyWithDate activity_id date) decodeRecord


postRecordBodyWithDate : Int -> String -> Http.Body
postRecordBodyWithDate activity_id date =
    Encode.object
        [ ( "activity_id", Encode.int activity_id )
        , ( "date", Encode.string date )
        ]
        |> Http.jsonBody



-- Activity Commands


decodeActivities : Decode.Decoder Activities
decodeActivities =
    Decode.list decodeActivity


decodeActivity : Decode.Decoder Activity
decodeActivity =
    Decode.map2 Activity
        (Decode.field "activity_id" Decode.int)
        (Decode.field "activity_name" Decode.string)


getActivities : Cmd Msg
getActivities =
  Http.request
    { method = "GET"
    , headers = []
    , url = baseUrl ++ "/activities"
    , body = emptyBody
    , expect = Http.expectJson LoadActivities decodeActivities
    , timeout = Nothing
    , tracker = Nothing
    }



-- Time Thingy


getNow : Cmd Msg
getNow =
    Http.get { url = baseUrl ++ "/now"
            , expect = Http.expectJson GetNow decodeNow
            }


decodeNow : Decode.Decoder Now
decodeNow =
    Decode.map5 Now
        (Decode.field "year" Decode.int)
        (Decode.field "month" Decode.string)
        (Decode.field "week" Decode.int)
        (Decode.field "day" Decode.string)
        (Decode.field "dayOfMonth" Decode.int)



-- Dates


getDates : Cmd Msg
getDates =
    Http.get {
    url = baseUrl ++ "/dates"
    , expect = Http.expectJson GetDates decodeDates
    }


decodeDates : Decode.Decoder Dates
decodeDates =
    Decode.list
        (Decode.map5 Date
            (Decode.field "localDate" Decode.string)
            (Decode.field "year" Decode.int)
            (Decode.field "month" Decode.string)
            (Decode.field "week" Decode.int)
            (Decode.field "dayOfWeek" Decode.string)
        )


-- END
