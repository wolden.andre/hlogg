module Views.Burger exposing (burger, burgerOpen)

import Html exposing (Html, button)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Messages exposing (Msg(..))
import Svg exposing (rect, svg)
import Svg.Attributes as SvgAttr
    exposing
        ( fill
        , height
        , rotate
        , rx
        , ry
        , viewBox
        , width
        , x
        , y
        )



-- NEXT: Lag burger open. Legg til onClick, set state, og transitionburger. Trenger kanskje ikke animation.


burgerOpen : Html Msg
burgerOpen =
    button [ class "burger", onClick CloseBurger ]
        [ svg
            [ width (String.fromFloat rectWidth)
            , height (String.fromFloat (rectHeight * 3 + spacing * 2))
            , viewBox ("0 0 " ++ String.fromFloat rectWidth ++ " " ++ String.fromFloat (rectHeight * 3 + spacing * 2))
            , SvgAttr.class "burgerSvg"
            ]
            [ rect
                [ x "0"
                , y "0"
                , width (String.fromFloat rectWidth)
                , height (String.fromFloat rectHeight)
                , rx cornerRadius
                , ry cornerRadius
                , fill rectColor
                , rotate "45 60 60"

                -- , SvgAttr.transform ("rotate(45 " ++ String.fromFloat rectWidth ++ " " ++ String.fromFloat (rectHeight / 2))
                , SvgAttr.transform "rotate(45 22 4) translate(9 9)"
                ]
                []
            , rect
                [ x "0"
                , y (String.fromFloat (rectHeight + spacing))
                , width (String.fromFloat rectWidth)
                , height (String.fromFloat rectHeight)
                , rx cornerRadius
                , ry cornerRadius
                , fill rectColor
                , SvgAttr.transform "rotate(-45 22 4) translate(-10 -5)"
                ]
                []
            ]
        ]


burger : Html Msg
burger =
    button [ class "burger", onClick OpenBurger ]
        [ svg
            [ width (String.fromFloat rectWidth)
            , height (String.fromFloat (rectHeight * 3 + spacing * 2))
            , viewBox ("0 0 " ++ String.fromFloat rectWidth ++ " " ++ String.fromFloat (rectHeight * 3 + spacing * 2))
            , SvgAttr.class "burgerSvg"
            ]
            [ rect
                [ x "0"
                , y "0"
                , width (String.fromFloat rectWidth)
                , height (String.fromFloat rectHeight)
                , rx cornerRadius
                , ry cornerRadius
                , fill rectColor
                ]
                []
            , rect
                [ x "0"
                , y (String.fromFloat (rectHeight + spacing))
                , width (String.fromFloat rectWidth)
                , height (String.fromFloat rectHeight)
                , rx cornerRadius
                , ry cornerRadius
                , fill rectColor
                ]
                []
            , rect
                [ x "0"
                , y (String.fromFloat ((rectHeight + spacing) * 2))
                , width (String.fromFloat rectWidth)
                , height (String.fromFloat rectHeight)
                , rx cornerRadius
                , ry cornerRadius
                , fill rectColor
                ]
                []
            ]
        ]


rectColor : String
rectColor =
    "white"


spacing : Float
spacing =
    rectHeight * 0.8


rectWidth : Float
rectWidth =
    45


rectHeight : Float
rectHeight =
    8


cornerRadius : String
cornerRadius =
    "0"
