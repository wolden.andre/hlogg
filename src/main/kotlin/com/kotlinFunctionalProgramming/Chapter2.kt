package com.kotlinFunctionalProgramming

import kotlin.collections.List

fun abs(int: Int): Int =
    when {
        int < 0 -> -1 * int
        else -> int
    }

fun formatAbs(x: Int): String =
    "The absolute value of $x is ${abs(x)}"

fun factorial(i: Int): Int =
    when (i) {
        1 -> 1
        else -> i * factorial(i - 1)
    }

fun formatFactorial(x: Int) =
    "The factorial of $x is ${factorial(x)}"

fun factorialTailrec(i: Int): Int {
    tailrec fun go(j: Int, acc: Int): Int =
        if (j <= 0) acc
        else go(j - 1, acc * j)
    return go(i, 1)
}

fun formatResult(x: Int, formattingFunction: (Int) -> Int, string: String) =
    string.format(x, formattingFunction(x))

// Exercise 2.1
fun fib(n: Int): Int {
    tailrec fun go(count: Int, current: Int, next: Int): Int =
        if (count == 1 )
            current
        else go(count -1, next, current + next)
    return go(n, 0, 1)
}

fun fib2(i: Int): Int {
    tailrec fun go(cnt: Int, curr: Int, nxt: Int): Int =
        if (cnt == 0)
            curr
        else go(cnt - 1, nxt, curr + nxt)
    return go(i, 0, 1)
}


fun findFirstString(listOfStrings: List<String>, string: String): Int {

    tailrec fun loop(index: Int): Int =
        when {
            index >= listOfStrings.size -> -1
            listOfStrings[index] == string -> index
            else -> loop(index + 1)
        }

    return loop(0)
}

fun <A> findFirst(ss: Array<A>, p: (A) -> Boolean): Int {
    tailrec fun loop(index: Int): Int =
        when {
            index >= ss.size -> -1
            p(ss[index]) -> index
            else -> loop(index + 1)
        }
    return loop(0)
}

fun <A, B, C> partialIt(a: A, f: (A, B) -> C): (B) -> C =
    { b -> f(a, b) }

fun <A, B, C> curry(f: (A, B) -> C): (A) -> (B) -> C =
    { a: A -> { b -> f(a, b) } }

fun <A, B, C> unCurry(g: (A) -> (B) -> C): (A, B) -> C =
    { a: A, b: B -> g(a)(b)}

fun <A, B, C> compose(f: (A) -> B, g: (B) -> C): (A) -> C =
    { a: A -> g(f(a)) }




