package com.andrewolden

import org.springframework.stereotype.Component

@Component
object EnvConf {
    val TEST_ENVIRONMENT_VARIABLE: String = System.getenv("AFROMAN") ?: "afroman is not an env variable. This is default."

    val CURRENT_ENVIRONMENT: String? = System.getenv("CURRENT_ENVIRONMENT")

    val DATABASE_URL: String = System.getenv("DATABASE_URL") ?: "jdbc:postgresql://localhost:5432/"
    val DATABASE_NAME: String = System.getenv("DATABASE_NAME") ?: "shiny-hlogg-db-local"
    val DATABASE_USERNAME: String = System.getenv("DATABASE_USERNAME") ?: "postgres"
    val POSTGRES_USER_PASSWORD: String = System.getenv("DATABASE_PASSWORD") ?: "no_password_set"
    val INSTANCE_CONNECTION_NAME: String = System.getenv("INSTANCE_CONNECTION_NAME") ?: "jdbc:postgresql://localhost:5432/"
    val SOCKET_FACTORY: String = System.getenv("SOCKET_FACTORY") ?: "no_socket_factory_set"
}


