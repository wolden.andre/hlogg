package com.andrewolden.hobby

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDate

data class Activity(val activity_id: Long, val activity_name: String)
data class Record(val id: Int, val date: LocalDate, val week: Int, val activity: Activity)


data class NewActivityRequestBody(@JsonProperty("activity_name") val activity_name: String)
data class DeleteActivityRequestBody(@JsonProperty("activity_id") val activity_id: Long)

data class NewRecordRequestBody(
    @JsonProperty("activity_id") val activity_id: Int,
    @JsonProperty("date") val date: LocalDate
)
