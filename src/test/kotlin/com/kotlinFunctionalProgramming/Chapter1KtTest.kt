package com.kotlinFunctionalProgramming

import kotlin.test.assertEquals
import kotlin.test.Test

class Chapter1KtTest {

    private val cafe = Cafe()
    private val cc = CreditCard("Jim Raynor", 1234)

    @Test fun f() {
        val coffee = cafe.buyCoffee(cc)
        assertEquals(coffee.first.price, 49.90f)
    }

    @Test
    fun `buy several coffees`() {
        val coffees = cafe.buyCoffees(cc, 8)
        assertEquals(399.19998f, coffees.second.amount)
    }

    @Test
    fun `coalesce test`() {
        val ccOle = CreditCard("Ole", 1234)
        val ccDole = CreditCard("Dole", 4321)

        val charge1 = Charge(ccOle, 10f)
        val charge3 = Charge(ccOle, 10f)
        val charge2 = Charge(ccOle, 10f)
        val charge4 = Charge(ccOle, 10f)


    }
}
