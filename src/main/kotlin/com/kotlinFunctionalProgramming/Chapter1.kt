package com.kotlinFunctionalProgramming

import java.lang.Exception
import kotlin.collections.List

data class CreditCard(val owner: String, val ccNumber: Long)


class Cafe {
    fun buyCoffee(cc: CreditCard): Pair<Coffee, Charge> {
        val cup = Coffee()
        return Pair(cup, Charge(cc, cup.price))
    }
    fun buyCoffees(cc: CreditCard, n: Int): Pair<List<Coffee>, Charge> {
        val purchases: List<Pair<Coffee, Charge>> = List(n) { buyCoffee(cc) }
        val (coffees: List<Coffee>, charges: List<Charge>) = purchases.unzip()
        return Pair(coffees, charges.reduce { c1: Charge, c2: Charge -> c1.combine(c2)})
    }
}

data class Coffee (val price: Float = 49.90f)

data class Charge(val cc: CreditCard, val amount: Float){
    fun combine(other: Charge) =
        if (other.cc == cc)
            Charge(cc, amount + other.amount)
        else throw Exception("Cannot combine charges to different cards")
}

fun List<Charge>.coalesce(): List<Charge> =
    this.groupBy { it.cc }
        .map { entry: Map.Entry<CreditCard, List<Charge>> -> entry.value.reduce { c1, c2 -> c1.combine(c2)} }

