package com.andrewolden.go

import com.andrewolden.utilities.go
import org.junit.Test
import kotlin.test.assertEquals

internal class GoKtTest {
    @Test
    fun `pipe stuff, or now, go stuff`() {
        val list: List<Int> = listOf(1, 2, 3, 4)

        val addFive: (Int) -> Int = { a: Int -> a + 5}

        fun goDoMore(list: List<Int>): Int =
            list.first() go addFive

        assertEquals(goDoMore(list), 6)

        fun useLetSomehow(s: String) =
            "Andre"
                .let { it + " " + "Wolden" }
                .let { s.length }

        assertEquals(useLetSomehow("abc"), 3)

    }
}
