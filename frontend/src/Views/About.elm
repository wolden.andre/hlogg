module Views.About exposing (viewAbout)

import Html exposing (Html, div, text)
import Messages exposing (Msg(..))
import Models.Model exposing (Model)


viewAbout : Model -> Html Msg
viewAbout model =
    div [] [ text "About" ]
