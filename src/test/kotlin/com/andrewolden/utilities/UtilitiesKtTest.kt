package com.andrewolden.utilities

import java.time.LocalDate
import kotlin.test.assertEquals
import kotlin.test.Test

internal class UtilitiesKtTest {

    @Test
    fun `generate list of LocalDates from a start and an end LocalDate`() {
        val start = LocalDate.of(2000, 1, 1)
        val end = LocalDate.of(2000, 1, 5)
        val actual: List<LocalDate> = populateLocalDateListFromRange(start, end)

        val expected: List<LocalDate> =
            listOf(
                LocalDate.of(2000, 1, 1),
                LocalDate.of(2000, 1, 2),
                LocalDate.of(2000, 1, 3),
                LocalDate.of(2000, 1, 4),
                LocalDate.of(2000, 1, 5)
            )

        assertEquals(expected, actual)
    }

    @Test
    fun `test av regex funksjoner for å lese file extenstion`() {
        assertEquals(true, isJs("asdf.js"))
        assertEquals(false, isJs("asdf.jsx"))
    }

    @Test
    fun `what does getenv return when nonexistent?`() {
        assertEquals("HLOGG_LOCAL", getEnvironment())
    }
}
