package com.kotlinFunctionalProgramming


import kotlin.test.assertEquals
import kotlin.test.Test

internal class ChapterTwoKtTest {
    @Test
    fun `factorial is correct for integers larger than zero`() {
        assertEquals(factorial(1), 1)
        assertEquals(factorial(2), 2)
        assertEquals(factorial(3), 6)
        assertEquals(factorial(7), 5040)
    }

    @Test
    fun `factorialTailrec is correct for integers larger than zero`() {
        assertEquals(factorialTailrec(1), 1)
        assertEquals(factorialTailrec(2), 2)
        assertEquals(factorialTailrec(3), 6)
        assertEquals(factorialTailrec(7), 5040)
    }

    @Test
    fun `get 1 fibonacci number`() {



        //41
        assertEquals(fib(1), 0)
        assertEquals(fib(2), 1)
        assertEquals(fib(3), 1)
        assertEquals(fib(4), 2)
        assertEquals(fib(5), 3)
        assertEquals(fib(6), 5)
        assertEquals(fib(7), 8)
        assertEquals(fib(8), 13)
        assertEquals(fib(9), 21)
        assertEquals(fib(10), 34)
        assertEquals(fib(11), 55)
        assertEquals(fib(12), 89)
        assertEquals(fib(13), 144)
    }

    @Test
    fun `format result`() {
        assertEquals(formatResult(3, ::factorial, "The factorial of %d is %d"), "The factorial of 3 is 6")
    }

    @Test
    fun `find a string in a list of strings`() {
        assertEquals(findFirstString(listOf("a", "b", "c"), "c"), 2)
        assertEquals(findFirstString(listOf("a", "b", "c"), "d"), -1)
    }

    @Test
    fun `partial tests`() {
        val fivePlus: (Int) -> Int = partialIt(5, { a: Int, b: Int -> a + b })

        assertEquals(fivePlus(6), 11)
    }

    @Test
    fun `i like curry`() {
        val leggSammen: (Int, Int) -> Int = { i: Int, j: Int -> i + j }
        val curryLeggSammen: (Int) -> (Int) -> Int = curry(leggSammen)
        val plussPaEn = curryLeggSammen(1)
        assertEquals(plussPaEn(4), 5)
    }

    @Test
    fun `i dont like curry afterall`() {

        val leggSammen: (Int, Int) -> Int = { i: Int, j: Int -> i + j }
        val curryLeggSammen: (Int) -> (Int) -> Int = curry(leggSammen)
        val leggSammenUncurried: (Int, Int) -> Int = unCurry(curryLeggSammen)
        assertEquals(leggSammenUncurried(4, 2), 6)
    }

}
