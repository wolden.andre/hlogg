package com.andrewolden.training

import arrow.core.Some
import com.andrewolden.repository.Repository
import com.andrewolden.repository.SqlDescription
import com.andrewolden.repository.runSqlAndDecodeResultSet
import com.google.api.gax.paging.Page
import com.google.cloud.storage.Blob
import com.google.cloud.storage.Bucket
import com.google.cloud.storage.Storage
import com.google.cloud.storage.StorageOptions
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.util.concurrent.atomic.AtomicLong
import javax.sql.DataSource


data class Greeting(val id: Long, val content: String)
data class SqlTest(val test_id: Int, val test_name: String)

fun decodeSqlTest(res: ResultSet): List<SqlTest> {
    val list: MutableList<SqlTest> = mutableListOf()
    while (res.next()) {
        list.add(
            SqlTest(res.getInt(1), res.getString(2))
        )
    }
    return list.toList()
}


@RestController
@RequestMapping("/api/training")
class TrainingController {

    val counter: AtomicLong = AtomicLong()

    @GetMapping("/greeting")
    fun testGreeting(@RequestParam(value = "name", defaultValue = "World") name: String): Greeting =
        Greeting(counter.incrementAndGet(), "Hello, $name")

    @GetMapping("/sqltest")
    fun sqlTest(): List<SqlTest> {

        val list = runSqlAndDecodeResultSet<List<SqlTest>>(
            SqlDescription<List<SqlTest>>(
                "SELECT * FROM sqltest;",
                ::decodeSqlTest
            )
        )

        return when (list) {
            is Some -> list.t
            else -> emptyList()
        }
    }

    @GetMapping("/iamGcpTest")
    fun testGcpIamThing(): String {
        val storage: Storage = StorageOptions.getDefaultInstance().service

        println("Buckets:")

        val buckets: Page<Bucket> = storage.list()

        val mutableListOfBuckets = mutableListOf<String>()

        for (bucket: Bucket in buckets.iterateAll()) {
            println(bucket.toString())
            mutableListOfBuckets.add(bucket.toString())

            if (bucket.name == "just-some-random-stuff-in-a-bucket") {
                val storage1: Storage = bucket.storage
                val blob: Blob = bucket.get("DB_PASSWORD")
                val content: ByteArray = blob.getContent()
                return content.toString()

            }
        }
        return "nothing from bucket content"
    }

    @GetMapping("/getSqlTestGcp")
    fun getSqlTestGcp(): List<SqlTest> {

        val dataSource: DataSource = Repository.DATA_SOURCE

        try {
            val connection: Connection = dataSource.getConnection()
            val statement = connection.createStatement()
            val query = "SELECT * FROM testtable;"
            val resultSet: ResultSet = statement.executeQuery(query)
            val sqlTestList: MutableList<SqlTest> = mutableListOf()

            while (resultSet.next()) {
                val sqlTest = SqlTest(
                    resultSet.getInt(1),
                    resultSet.getString(2)
                )
                sqlTestList.add(sqlTest)
            }

            resultSet.close()
            statement.close()
            connection.close()

            return sqlTestList
        } catch (e: SQLException) {
            val message = "GetSqlTestGcp failed with exception: $e"
            println(message)
            return listOf(SqlTest(1, message))
        }
    }
}
