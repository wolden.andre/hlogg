package com.andrewolden.hobby

import arrow.core.Option
import arrow.core.Some
import com.andrewolden.utilities.populateLocalDateListFromRange
import org.springframework.web.bind.annotation.*
import java.time.LocalDate

@RestController
@RequestMapping("/api/hobby")
class DateResource {

    @CrossOrigin
    @GetMapping("/dates")
    fun getDates(): List<DateResponse> =
        when (val startDate: Option<LocalDate> = getOldestDate()) {
            is Some -> {
                val endDate = LocalDate.now()
                val listOfLocalDates: List<LocalDate> = populateLocalDateListFromRange(startDate.t, endDate)
                listOfLocalDates.map {date: LocalDate -> DateResponse(date)}
            }
            // TODO: Handle error in some sensible way
            else -> emptyList()
        }

    @CrossOrigin
    @GetMapping("/now")
    fun getNow(): TodayResponse =
        TodayResponse(LocalDate.now())



}
