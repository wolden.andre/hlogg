package com.andrewolden.test

data class TestType(
    val id: Long,
    val string: String
)
