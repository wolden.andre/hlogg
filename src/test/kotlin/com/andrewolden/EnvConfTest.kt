package com.andrewolden

import kotlin.test.Test
import kotlin.test.assertEquals

internal class EnvConfTest {

    @Test
    fun getINSTANCE_CONNECTION_NAME() {
        assertEquals(EnvConf.INSTANCE_CONNECTION_NAME, "no_instance_connection_name_set")
    }
}
