package com.andrewolden.utilities

import kotlin.collections.mutableListOf
import kotlin.collections.MutableList
import java.time.LocalDate

enum class OperationResult {
    SUCCESS, FAILURE
}

enum class ResponseStatus {
    SUCCESS, FAILURE
}

data class ResponseBody<R, E> (
    val responseStatus: ResponseStatus,
    val response: R?,
    val error: E?
)

data class Error (
    val errorCode: Int,
    val errorMessage: String
)

val ErrorDeletingActivity = Error(1, "Could not delete activity.")
val ErrorGetAllActivities = Error(2, "Failed during getAllActivities")
val ErrorInsertNewActivity = Error(2, "Failed during insertNewActivity")

fun populateLocalDateListFromRange(start: LocalDate, end: LocalDate): List<LocalDate> {
    val list: MutableList<LocalDate> = mutableListOf()
    fun go(date: LocalDate, endDate: LocalDate, list: MutableList<LocalDate>): List<LocalDate> {
        val nextDay: LocalDate = date.plusDays(1)
        list.add(date)
        return when {
            date >= endDate -> list
            else -> go(nextDay, endDate, list)
        }
    }
    return go(start, end, list)
}

fun isJs(string: String): Boolean = Regex(".*(.js)$").matches(string)
fun isCss(string: String): Boolean = Regex(".*(.css)$").matches(string)

fun getEnvironment(): String =
    try {
        System.getenv("HLOGG_ENVIRONMENT")
    } catch (e: Exception) {
        "HLOGG_LOCAL"
    }
