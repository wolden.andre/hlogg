package com.andrewolden.repository

import arrow.core.Option
import com.andrewolden.EnvConf
import com.andrewolden.HloggLogger
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import javax.sql.DataSource


class Repository {
    companion object {
//        val HEROKU_POOL: DataSource = getHerokuPool()
//        val GCP_POOL: DataSource = getGcpPool()
//        val LOCAL_POOL: DataSource = getLocalPool()
//
        val DATA_SOURCE: DataSource = initiateDataSource()
    }
}

fun initiateDataSource(): DataSource = when (EnvConf.CURRENT_ENVIRONMENT) {
    "GCP" -> getGcpPool()
    "HEROKU" -> getHerokuPool()
    "LOCAL" -> getLocalPool()
    else -> throw Error("Environment variable \"CURRENT_ENVIRONMENT\" must be set to use correct DataSource pool.")
}

data class SqlDescription<A>(
    val query: String,
    val decoder: (res: ResultSet) -> A
)

fun <A> runSqlAndDecodeResultSet(des: SqlDescription<A>): Option<A> =
    try {
        val datasource = Repository.DATA_SOURCE
        val connection: Connection = datasource.connection
        val statement = connection.createStatement()
        val resultSet: ResultSet = statement.executeQuery(des.query)
        val result: A = des.decoder(resultSet)

        resultSet.close()
        statement.close()
        connection.close()
        Option.just(result)
    } catch (e: SQLException) {
        Option.empty()
    }


fun getHerokuPool(): DataSource {
    val config = HikariConfig()
    val HEROKU_JDBC_DATABASE_URL = System.getenv("JDBC_DATABASE_URL").also {
        if (it == null) {
            HloggLogger.LOG.severe("In getDataSource(). JDBC_DATABASE_URL environment variable is not set!!")
        }
    }
    config.jdbcUrl = HEROKU_JDBC_DATABASE_URL.also {
        val logMessage = "In getDataSource(). Setting jdbc url: $it"
        HloggLogger.LOG.info(logMessage)
    }
    config.driverClassName = "org.postgresql.Driver"
    return HikariDataSource(config)
}

fun getGcpPool(): DataSource {
    val config = HikariConfig()
    config.jdbcUrl = String.format(
        "jdbc:postgresql:///%s",
        EnvConf.DATABASE_NAME
    );
    config.username = EnvConf.DATABASE_USERNAME; // e.g. "root", "postgres"
    config.password = EnvConf.POSTGRES_USER_PASSWORD; // e.g. "my-password"

    config.addDataSourceProperty("socketFactory", EnvConf.SOCKET_FACTORY);
    config.addDataSourceProperty(
        "cloudSqlInstance",
        EnvConf.INSTANCE_CONNECTION_NAME
    );

    config.maximumPoolSize = 5
    config.minimumIdle = 5
    config.connectionTimeout = 10000
    config.idleTimeout = 600000
    config.maxLifetime = 1800000

    val pool: DataSource =
        HikariDataSource(config)
    return pool
}

fun getLocalPool(): DataSource {
    val config = HikariConfig()
    config.jdbcUrl = EnvConf.DATABASE_URL + EnvConf.DATABASE_NAME.also {
        val logMessage = "In getDataSource(). Setting jdbc url: $it"

        HloggLogger.LOG.info(logMessage)

    }
    config.driverClassName = "org.postgresql.Driver"
    return HikariDataSource(config)
}

