port module Main exposing (main, toJs)

import Browser
import Commands exposing (..)
import Messages exposing (Msg)
import Models.Model exposing (Model)
import Models.SubModelAddRecordOnDate exposing (..)
import Models.Types exposing (..)
import Update exposing (update)
import View exposing (view)


port toJs : String -> Cmd msg


main : Program Int Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view =
            \m ->
                { title = "Hobby Log"
                , body = [ view m ]
                }
        , subscriptions = \_ -> Sub.none
        }


init : Int -> ( Model, Cmd Msg )
init flags =
    ( { debugMessage = "Number, from index.js: "
      , records = []
      , activities = []
      , addRecordBlockState = PlusSign
      , now = Nothing
      , subModelAddRecordOnDate = initSubModelAddRecordOnDate
      , dates = Nothing
      , page = Loading
      , burgerStatus = Closed
      , dateToOpenListOfActivitiesFor = Nothing
      }
    , Cmd.batch
        [ Commands.getRecords
        , Commands.getActivities
        , Commands.getNow
        , Commands.getDates
        ]
    )


initSubModelAddRecordOnDate : SubModelAddRecordOnDate
initSubModelAddRecordOnDate =
    { viewState = Initial
    , activity_id = Nothing
    , date = Nothing
    }
