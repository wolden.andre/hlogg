FROM registry.gitlab.com/wolden.andre/hlogg:0.1.0

EXPOSE 8080

CMD ["/usr/bin/java", "-jar", "app.jar"]

