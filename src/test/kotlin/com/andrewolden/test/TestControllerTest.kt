package com.andrewolden.test

import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.expect


internal class TestControllerTest {

    val testController: TestController = TestController()

//    @Test
//    fun testApplicationPropertiesTest() {
//        val result = testController.testApplicationProperties()
//        assertEquals(result, "asdf")
//    }

    @Test
    fun testEnvironmentVariableTest() {
        val result = testController.testEnvironmentVariable()
        assertEquals(result, "because I got high")
    }

    @Test
    fun testCreateTable(){
        val result = testController.testDatabase()
        assertEquals(result, "Returning after successfully creating table.")
    }
}
