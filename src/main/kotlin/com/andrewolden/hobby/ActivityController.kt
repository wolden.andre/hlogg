package com.andrewolden.hobby

import arrow.core.Some
import com.andrewolden.utilities.OperationResult
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/hobby")
class ActivityController {

    @CrossOrigin
    @GetMapping("/activities")
    fun allActivities(): List<Activity> =
        when (val a = getAllActivities()) {
            is Some -> a.t
            else -> emptyList()
        }

    @PutMapping("/activities")
    fun newActivity(@RequestBody newActivityRequestBody: NewActivityRequestBody): List<Activity> =
        insertNewActivityAndReturnListOfActivities(newActivityRequestBody.activity_name)

    @DeleteMapping("activities")
    fun delActivity(@RequestBody deleteActivityRequestBody: DeleteActivityRequestBody): List<Activity> =
        when (val res: OperationResult = deleteActivity(deleteActivityRequestBody.activity_id)) {
            OperationResult.SUCCESS -> getAllActivities()
                .let {
                    when (it) {
                        is Some -> it.t
                        else -> emptyList()
                    }
                }
            OperationResult.FAILURE -> emptyList()
        }
}
