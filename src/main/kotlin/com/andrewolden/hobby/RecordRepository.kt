package com.andrewolden.hobby

import arrow.core.Option
import com.andrewolden.EnvConf
import com.andrewolden.HloggLogger
import com.andrewolden.repository.Repository
import com.andrewolden.repository.SqlDescription
import com.andrewolden.repository.runSqlAndDecodeResultSet
import java.sql.*
import java.time.LocalDate
import java.time.temporal.WeekFields
import javax.sql.DataSource

val getAllRecordsQuery: String =
    "SELECT records.record_id, records.date, records.week, records.activity_id, " +
            "activities.activity_id, activities.activity_name " +
            "FROM records " +
            "INNER JOIN activities ON (records.activity_id=activities.activity_id);"

/*
            "SELECT records.record_id, records.date, records.week, activities.activity_id, activities.activity_name " +
                    "FROM records, activities WHERE " +
                    "records.record_id=${record_id} AND records.activity_id=activities.activity_id;"

 */

fun resultsetDecoderGetAllRecordsSql(res: ResultSet): List<Record> {
    val mutableListOfRecords = mutableListOf<Record>()
    while (res.next()) {
        val activity = Activity(res.getLong("activity_id"), res.getString("activity_name"))
        val record = Record(
            res.getInt("record_id"),
            res.getDate("date").toLocalDate(),
            res.getInt("week"),
            activity
        )
        mutableListOfRecords.add(record)
    }
    return mutableListOfRecords.toList()
}

val SQL_DESCRIPTION_RECORDS: SqlDescription<List<Record>> =
    SqlDescription<List<Record>>(
        getAllRecordsQuery,
        ::resultsetDecoderGetAllRecordsSql
    )

fun getAllRecords(): Option<List<Record>> =
    runSqlAndDecodeResultSet(SQL_DESCRIPTION_RECORDS)

fun saveNewRecord(record: NewRecordRequestBody): Option<Record> {
    val week: Int = record.date.get(WeekFields.ISO.weekOfYear())
    try {

        val dataSource: DataSource = Repository.DATA_SOURCE
        val connection: Connection = dataSource.connection

        val sql: String = String.format(
            "INSERT INTO records (date, week, activity_id)" +
                    "VALUES (?,?,?);"
        )
        val date: LocalDate = record.date
        val dateFormatted = Date.valueOf(date)

        val preparedStatement: PreparedStatement = connection.prepareStatement(
            sql,
            Statement.RETURN_GENERATED_KEYS
        )
        preparedStatement.setDate(1, dateFormatted)
        preparedStatement.setInt(2, week)
        preparedStatement.setInt(3, record.activity_id)
        val affectedRows = preparedStatement.executeUpdate()

        if (affectedRows == 0) {
            throw SQLException("SAVING NEW RECORD FAILED")
        }

        val generatedKeys = preparedStatement.generatedKeys
        generatedKeys.next()
        val generated_record_id = generatedKeys.getInt(1)
        val maybeRecord = selectRecordById(record_id = generated_record_id)
        connection.close()
        return maybeRecord

    } catch (e: SQLException) {
        HloggLogger.LOG.warning("Failed to save Record")
        return Option.empty()
    }
}

fun deleteRecordById(record_id: Int) {
    val dataSource: DataSource = Repository.DATA_SOURCE
    val connection: Connection = dataSource.connection
    val sql: String = String.format(
        "INSERT INTO records (date, week, activity_id)" +
                "VALUES (?,?,?);"
    )
}

fun selectRecordById(record_id: Int): Option<Record> {
    try {
        val dataSource: DataSource = Repository.DATA_SOURCE
        val connection: Connection = dataSource.connection
        val queryString: String = String.format(
            "SELECT records.record_id, records.date, records.week, activities.activity_id, activities.activity_name " +
                    "FROM records, activities WHERE " +
                    "records.record_id=${record_id} AND records.activity_id=activities.activity_id;"
        )

        val statement: Statement = connection.createStatement()

        val resultSet: ResultSet = statement.executeQuery(queryString)

        resultSet.next()

        val record = Record(
            resultSet.getInt(1),
            resultSet.getDate(2).toLocalDate(),
            resultSet.getInt(3),
            Activity(resultSet.getInt(4).toLong(), resultSet.getString(5))
        )

        return Option.just(record)

    } catch (e: SQLException) {
        HloggLogger.LOG.warning("Failed to save Record")
        return Option.empty()
    }
}


