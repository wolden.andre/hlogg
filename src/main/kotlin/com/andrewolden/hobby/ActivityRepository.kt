package com.andrewolden.hobby

import com.andrewolden.utilities.*
import com.andrewolden.utilities.OperationResult.*
import java.sql.*
import arrow.core.Option
import arrow.core.Option.Companion.empty
import arrow.core.Some
import com.andrewolden.EnvConf
import com.andrewolden.HloggLogger
import com.andrewolden.repository.SqlDescription
import com.andrewolden.repository.runSqlAndDecodeResultSet

fun getAllActivities(): Option<List<Activity>> {
    try {
        val query = "SELECT * FROM activities"

        fun activitiesDecoder(resultSet: ResultSet): List<Activity> {
            val activitiesList: MutableList<Activity> = mutableListOf<Activity>()

            while (resultSet.next()) {
                val activity = Activity(
                    resultSet.getInt(1).toLong(),
                    resultSet.getString(2)
                )
                activitiesList.add(activity)
            }
            return activitiesList.toList()
        }

        val sqlDescription: SqlDescription<List<Activity>> =
            SqlDescription<List<Activity>>(query, ::activitiesDecoder)

        val maybeResult: Option<List<Activity>> =
            runSqlAndDecodeResultSet(sqlDescription)

        return maybeResult
    } catch (e: SQLException) {
        HloggLogger.LOG.warning("ActivityRepository. getAllActivities. Catch. ERROR: $e")
        return empty()
    }
}

fun insertNewActivity(activityName: String): OperationResult {
    try {
        val connection = DriverManager.getConnection(EnvConf.DATABASE_URL)
        val sql = String.format(
            "INSERT INTO activities (activity_name)" +
                    "VALUES (?);"
        )
        val preparedStatement = connection.prepareStatement(sql)
        preparedStatement.setString(1, activityName)

        val affectedRows = preparedStatement.executeUpdate()

        preparedStatement.close()
        connection.close()
        return when (affectedRows) {
            0 -> {
                FAILURE

            }
            else -> {
                SUCCESS
            }
        }
    } catch (e: SQLException) {
        HloggLogger.LOG.warning("Could not save new activity:")
        HloggLogger.LOG.warning(e.toString())
        return FAILURE
    }
}

fun deleteActivity(activity_id: Long): OperationResult {
    try {
        val connection = DriverManager.getConnection(EnvConf.DATABASE_URL)
        val sql = "DELETE FROM activities WHERE activity_id=${activity_id.toInt()};"
        val preparedStatement = connection.prepareStatement(sql)
        val affectedRows = preparedStatement.executeUpdate()

        preparedStatement.close()
        connection.close()

        return when (affectedRows) {
            0 -> {
                FAILURE

            }
            else -> {
                SUCCESS
            }
        }

    } catch (e: SQLException) {
        HloggLogger.LOG.warning("Could not delete activity:")
        HloggLogger.LOG.warning(e.toString())
        return FAILURE
    }
}

fun insertNewActivityAndReturnListOfActivities(activityName: String): List<Activity> =
    insertNewActivity(activityName).let {
        return when (it) {
            SUCCESS -> getAllActivities().let {
                when (it) {
                    is Some -> it.t
                    else -> emptyList()
                }
            }
            FAILURE -> emptyList()
        }
    }
