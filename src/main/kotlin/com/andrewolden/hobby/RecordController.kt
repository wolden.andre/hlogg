package com.andrewolden.hobby

import arrow.core.Option
import arrow.core.Option.Companion.empty
import arrow.core.Some
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/hobby")
class RecordController {

    @CrossOrigin
    @GetMapping("/records")
    fun allActivities(): List<Record> =
        when (val a = getAllRecords()) {
            is Some -> a.t
            else -> emptyList()
        }

    @CrossOrigin
    @PostMapping("/records/new")
    fun postNewRecord(@RequestBody newRecord: NewRecordRequestBody): Option<Record> =
        saveNewRecord(newRecord)
}
