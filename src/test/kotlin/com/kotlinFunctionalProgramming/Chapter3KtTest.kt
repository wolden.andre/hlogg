package com.kotlinFunctionalProgramming

import kotlin.test.assertEquals
import kotlin.test.Test

data class Stuff(val some: String, val stuff: Int)


internal class Chapter3KtTest {
    @Test
    fun `start using the new list type implementation`() {
        val emptyList: List<Nothing> =
            Nil
        val stuff1 = Stuff("a", 1)
        val stuff2 = Stuff("b", 2)
        val aListWithStuff: List<Stuff> =
            List.of(stuff1, stuff2)
        val aListOfInts: List<Int> =
            List.of(1, 2, 3)
        val aListFromCons: List<Stuff> = Cons(stuff1, Cons(stuff2, Nil))
        assertEquals(List.sum(aListOfInts), 6)
    }

    fun listToString(list: List<String>): String {
        tailrec fun go(bb: List<String>, string: String): String =
            when (bb) {
                is Nil -> string
                is Cons -> go(
                    List.tail(bb), string + List.head(
                        bb
                    ) + ", ")
            }
        return go(list, "")
    }

    @Test
    fun `test list features`() {
        val list: List<String> =
            List.of("1", "2", "2", "3")
        val tailOfList = List.tail(list)

        assertEquals(listToString(tailOfList), "2, 2, 3, ")
    }

    @Test
    fun `drop drops right amount of stuff`() {
        val listOfInts: List<Int> = List.of(10, 20, 30, 40)
        val reducedListOfInts = List.drop(1, listOfInts)
        assertEquals(reducedListOfInts, Cons(20, Cons(30, Cons(40, Nil))))
        assertEquals(List.drop(2, listOfInts), List.of(30, 40))
    }

    @Test
    fun `length of a list`() {
        val l = List.of(1, 2, 3, 4, 5)
        assertEquals(List.length(l), 5)

    }

}
