module Views.Statistics exposing (viewStatistics)

import Html exposing (Html, div, text)
import Messages exposing (Msg(..))
import Models.Model exposing (Model)


viewStatistics : Model -> Html Msg
viewStatistics model =
    div [] [ text "Statistics" ]
