package com.andrewolden.utilities


infix fun <T, R> T.go(func: (T) -> R) = func(this)

