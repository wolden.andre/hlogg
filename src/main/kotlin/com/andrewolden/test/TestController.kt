package com.andrewolden.test

import com.andrewolden.EnvConf
import com.andrewolden.repository.Repository
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.sql.DataSource


@RestController
@RequestMapping("/testing")
@Service
class TestController {

    @GetMapping("/test-endpoint")
    fun testEndpoint(): String {
        return "Kun et endepunkt som returnerer denne strengen. Ingen autentisering."
    }

//    @GetMapping("/test-application-properties")
//    fun testApplicationProperties(): String {
//        return EnvConf.TEST_VARIABLE
//    }

    @GetMapping("/test-environment-variable")
    fun testEnvironmentVariable(): String {
        return EnvConf.TEST_ENVIRONMENT_VARIABLE
    }

    @GetMapping("/test-database-create-table")
    fun testDatabase(): String {
        val dataSource: DataSource = Repository.DATA_SOURCE
        val result: String = createTestTable(dataSource)
        return result
    }
}
