package com.andrewolden

import com.andrewolden.repository.Repository
import org.flywaydb.core.Flyway
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Application

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)

    logEnvironmentVariables()
    runFlywayMigration()
}

private fun runFlywayMigration() {
    val flyway = Flyway.configure().dataSource(Repository.DATA_SOURCE)
        .load()
    flyway.baseline()
    flyway.migrate()
}

private fun logEnvironmentVariables() {
    HloggLogger.LOG.info("Environment variables currently in use:")
    HloggLogger.LOG.info("CURRENT_ENVIRONMENT: ${EnvConf.CURRENT_ENVIRONMENT}")
    HloggLogger.LOG.info("DATABASE_URL: ${EnvConf.DATABASE_URL}")
    HloggLogger.LOG.info("DATABASE_NAME: ${EnvConf.DATABASE_NAME}")
    HloggLogger.LOG.info("DATABASE_USERNAME: ${EnvConf.DATABASE_USERNAME}")
}
