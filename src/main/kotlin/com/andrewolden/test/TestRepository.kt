package com.andrewolden.test

import javax.sql.DataSource

fun createTestTable(dataSource: DataSource): String {
    try {
        val conn = dataSource.getConnection()
        val createTableStatement = conn.prepareStatement(
            "CREATE TABLE IF NOT EXISTS testtable ( "
                    + "id SERIAL NOT NULL, string CHAR(20) NOT NULL,"
                    + " PRIMARY KEY (id) );"
        )

        createTableStatement.execute()

        conn.close()

        return "Returning after successfully creating table."

    } catch (e: Exception) {
        return "TestController. testDatabase. Catch. ERROR: $e"

    }
}
