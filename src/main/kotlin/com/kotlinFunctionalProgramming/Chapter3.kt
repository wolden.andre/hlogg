package com.kotlinFunctionalProgramming

sealed class List<out A> {
    // helper functions
    companion object {
        fun <A> of(vararg aa: A): List<A> {
            val tail = aa.sliceArray(1 until aa.size)
            return if (aa.isEmpty()) Nil else Cons(
                aa[0],
                of(*tail)
            )
        }

        fun sum(ints: List<Int>): Int = when (ints) {
            is Nil -> 0
            is Cons -> ints.head + sum(
                ints.tail
            )
        }

        fun product(doubles: List<Double>): Double = when (doubles) {
            is Nil -> 1.0
            is Cons ->
                if (doubles.head == 0.0) 0.0
                else doubles.head * product(doubles.tail)
        }

        fun <A> tail(aa: List<A>): List<A> =
            when (aa) {
                is Nil -> throw IllegalStateException("Nil cannot have a `tail`")
                is Cons -> aa.tail
            }

        fun <A> head(aa: List<A>): A =
            when (aa) {
                is Nil -> throw IllegalStateException("Nil cannot have a `head`")
                is Cons -> aa.head
            }

        fun <A> setHead(a: A, aa: List<A>): List<A> =
            when (aa) {
                is Nil -> Cons(a, Nil)
                is Cons -> Cons(a, aa)
            }

        fun <A> drop(n: Int, l: List<A>): List<A> =
            if (n == 0) l
            else when (l) {
                is Nil -> throw Exception("aksjdn")
                is Cons -> drop(n - 1, List.tail(l))
            }

        fun <A> dropWhile(f: (A) -> Boolean, l: List<A>): List<A> =
            when (l) {
                is Nil -> l
                is Cons -> when (f(List.head(l))) {
                    true -> dropWhile(f, List.tail(l))
                    false -> l
                }
            }

        // e.g. List.append(l1, l2) -> ["a", "b", "c"] + ["d", "e", "f"] -> ["a", "b", "c", "d", "e", "f"]
        fun <A> append(l1: List<A>, l2: List<A>): List<A> =
            when (l1) {
                is Nil -> l2
                is Cons -> Cons(l1.head, append(l1.tail, l2))
            }


        fun <A> init(l: List<A>): List<A> =
            when (l) {
                is Nil -> Nil
                is Cons ->
                    when (l.tail) {
                        is Nil -> Nil
                        is Cons -> Cons(l.head, init(l.tail))
                    }
            }

        fun sumAgain(l: List<Int>): Int =
            when (l) {
                is Nil -> 0
                is Cons -> l.head + sumAgain(l.tail)
            }


        fun <A> empty(): List<A> = Nil

        fun <A> length(l: List<A>): Int =
            when (l) {
                is Nil -> 0
                is Cons -> 1 + length(l.tail)
            }


        // FOLD RIGHT
        // [1, 2, 3]
        // fun f(a: Int, b: Int) = a + b
        // C = 0
        // foldRight(l, f, c)
        // => f(1, f(2, f(3, 0)))
        // => 1 + 2 + 3 + 0 => 6
        fun <A, B> foldRight(l: List<A>, f: (A, B) -> B, c: B): B =
            when (l) {
                is Nil -> c
                is Cons -> f(l.head, foldRight(l.tail, f, c))
            }


        // FOLD LEFT
        // [1, 2, 3]
        // fun f(a: Int, b: Int) = a + b
        // c = 0
        // foldLeft([1, 2, 3], f, c

        // foldLeft(l, f, c)
        // => foldLeft([2, 3], f, 0+1)
        // => foldLeft([3], f, 1+2)
        // => foldLeft(Nil, f, 3+3)
        // => l is Nil => 6
        tailrec fun <A, B> foldLeft(l: List<A>, f: (B, A) -> B, c: B): B =
            when (l) {
                is Nil -> c
                is Cons -> foldLeft(l.tail, f, f(c, l.head))
            }

        /*

        l1 = [1, 2, 3]
        l2 = [4, 5, 6]
        f = {x: List<Int>, y: Int -> Cons(y, x)
        append2(l1, l2)

           = foldLeft(l1, f, l2) <----
           = foldLeft([2, 3], f, Cons(1, [4, 5, 6])
           = foldLeft([3], f, Cons(2, [1,4,5,6])
           = foldLeft(Nil, f, Cons(3, [2, 1, 4, 5, 6])
           = [3, 2, 1, 4, 5, 6]



        */

        fun sum2(l: List<Int>): Int =
            foldLeft(l, { a, b -> a + b }, 0)

        fun product2(l: List<Double>): Double =
            foldLeft(l, { a, b -> a * b }, 1.0)

        fun <A> length2(l: List<A>): Int =
            foldLeft(l, { acc, _ -> acc + 1 }, 0)

        fun <A> reverse(l: List<A>): List<A> =
            foldLeft(l, { acc: List<A>, new: A -> setHead(new, acc) }, empty())

        // f: (A, B) -> B
        // g: (B) -> B
        // foldLeft(List<A>, ( (B) -> B, A ) -> (B) -> B), (B) -> B) ->
        fun <A, B> foldRightL(xs: List<A>, z: B, f: (A, B) -> B): B {
            val h: (B) -> B = foldLeft(xs, { g: (B) -> B, a: A -> { b: B -> g(f(a, b)) } }, { b: B -> b })
            return h(z)
        }

        fun <A> append2(l1: List<A>, l2: List<A>): List<A> =
            foldLeft(l1, { x: List<A>, y: A -> Cons(y, x) }, l2)


    }
}

object Nil : List<Nothing>()

data class Cons<out A>(val head: A, val tail: List<A>) : List<A>()
