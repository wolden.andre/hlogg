package com.andrewolden.training

fun lamdaDoss(a: String, f: (String) -> String): String =
    "I'm doing nothing hehe."

val a: String = lamdaDoss(
    "Andre Wolden"
) { a: String -> a + ". yeah right"}
// conclusion: If only one lamda, then it can be moved outside the parenthesises
// `\o/´...makes sense ? is it THAT awesome ?
