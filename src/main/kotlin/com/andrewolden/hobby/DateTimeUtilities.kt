package com.andrewolden.hobby

import com.fasterxml.jackson.annotation.JsonAutoDetect
import java.time.LocalDate
import java.time.temporal.WeekFields
import arrow.core.Option
import com.andrewolden.repository.SqlDescription
import com.andrewolden.repository.runSqlAndDecodeResultSet
import java.sql.ResultSet


@JsonAutoDetect(
    fieldVisibility = JsonAutoDetect.Visibility.ANY,
    getterVisibility = JsonAutoDetect.Visibility.NONE,
    setterVisibility = JsonAutoDetect.Visibility.NONE,
    creatorVisibility = JsonAutoDetect.Visibility.NONE
)
data class DateResponse(val localDate: LocalDate) {
    private val year: Int = localDate.year
    private val month: String = localDate.month.toString()
    private val week: Int = localDate[WeekFields.ISO.weekOfYear()]
    private val dayOfWeek: String = localDate.dayOfWeek.toString()
}

@JsonAutoDetect(
    fieldVisibility = JsonAutoDetect.Visibility.ANY,
    getterVisibility = JsonAutoDetect.Visibility.NONE,
    setterVisibility = JsonAutoDetect.Visibility.NONE,
    creatorVisibility = JsonAutoDetect.Visibility.NONE
)
data class TodayResponse(val localDate: LocalDate) {
    private val year: Int = localDate.year
    private val month: String = localDate.month.toString()
    private val week: Int = localDate[WeekFields.ISO.weekOfYear()]
    private val day: String = localDate.dayOfWeek.toString()
    private val dayOfMonth: Int = localDate.dayOfMonth
}


val queryOldestRecord: String = "SELECT min(date) AS date FROM records;"

fun decodeOldestRecord(res: ResultSet): LocalDate {
    res.next()
    return res.getDate("date").toLocalDate()
}

val SQLTEMPLATE_OLDEST_DATE: SqlDescription<LocalDate> =
    SqlDescription(queryOldestRecord, ::decodeOldestRecord)

fun getOldestDate(): Option<LocalDate> =
    runSqlAndDecodeResultSet(SQLTEMPLATE_OLDEST_DATE)
