package com.andrewolden.FrontendApp

import com.andrewolden.utilities.isJs
import org.springframework.web.bind.annotation.*
import java.io.File
import java.net.URL
import javax.servlet.http.HttpServletResponse
import javax.websocket.server.PathParam

@RestController
@RequestMapping("/")
class StaticResourcesController {

    @GetMapping("/")
    fun getStaticRoot(): String = getIndexHtmlAsString()

    @GetMapping("/index.html")
    fun getStaticIndexHtml(): String = getIndexHtmlAsString()

    @GetMapping("/static/{filename}")
    fun getFile(@PathVariable filename: String, response: HttpServletResponse): ByteArray {
        try {

            when {
                isJs(filename) -> response.addHeader("Content-Type", "image/png")

            }

//            response.addHeader("Content-Type", "text/html")

            val file: ByteArray = StaticResourcesController::class.java
                .getResource("$filename")
                .readBytes()

            // com/andrewolden/FrontendApp/test.txt



            return file

        } catch (e: Exception) {
            val bytesToReturn = "Couldn't find file: $filename"
            println(bytesToReturn)
            bytesToReturn.toByteArray(Charsets.UTF_8)
            return byteArrayOf()
        }
    }

    fun getIndexHtmlAsString(): String =
        try {
            val indexhtml: String = StaticResourcesController::class.java
                .getResource("index.html")
                .readText()
            indexhtml

        } catch (e: Exception) {
            "File not found"
        }


}
